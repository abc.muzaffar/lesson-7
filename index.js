
const timer = document.getElementById('stopwatch');

var min = 0;
var sec = 0;
var key = 0;
var stoptime = true;
var timerValue = '';

start.onclick = function() {
    if (stoptime == true) {
        stoptime = false;
        timerCycle();
    }
}
pause.onclick = function() {
    if (stoptime == false) {
        stoptime = true;
    }
    key++;
    localStorage.setItem('key'+key, timerValue);

}

function timerCycle() {
    if (stoptime == false) {
        sec = parseInt(sec);
        min = parseInt(min);

        sec = sec + 1;

        if (sec == 60) {
            min = min + 1;
            sec = 0;
        }

        if (sec < 10 || sec == 0) {
            sec = '0' + sec;
        }
        if (min < 10 || min == 0) {
            min = '0' + min;
        }
        timerValue = min + ':' + sec;
        timer.innerHTML = timerValue;

        setTimeout("timerCycle()", 1000);
    }
}

reset.onclick = function() {
    timer.innerHTML = '00:00';
    min = 0;
    sec = 0;
    key = 0;
    if (stoptime == false) {
        stoptime = true;
    }
    localStorage.clear();
    
}